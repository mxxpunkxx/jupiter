<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Auth::routes(['verify' => true]);

Route::apiResources([
    'user' => 'API\UserController',
]);

Route::get('profile', 'API\UserController@profile');
Route::put('profile', 'API\UserController@updateProfile');
Route::get('find', 'API\UserController@findBox');

Route::get('trash', 'API\UserController@recycleBin')->name('trash');
Route::delete('trash/{id}', 'API\UserController@forceDelete')->name('forceDelete');
Route::put('trash/{id}', 'API\UserController@restoreTrashedUser')->name('restoreTrashedUser');
