<?php

namespace App\Http\Controllers\API;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Intervention\Image\Facades\Image;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

  
    public function index()
    {
        if (Gate::allows('isAdmin'))
        {
            return User::latest()->paginate(10);
        }else{
            return ['Message' => 'You are not authorized.'];
        }
    }

    public function recycleBin()
    {
        if (Gate::allows('isAdmin'))
        {
            return User::latest()->onlyTrashed()->paginate(10);
        }else{
            return ['Message' => 'You are not authorized.'];
        }
    }

    public function restoreTrashedUser($id)
    {
        if (Gate::allows('isAdmin'))
        {
            $trashedUser = User::withTrashed()->findOrfail($id);
            $trashedUser->restore();
            return ['message' => 'Deleted user restored successfully.'];
        }else{
            return ['Message' => 'You are not authorized.'];
        }
    }

    public function forceDelete($id)
    {
        if (Gate::allows('isAdmin'))
        {
            $trashedUser = User::withTrashed()->findOrfail($id);
            $trashedUser->forceDelete();
            return ['message' => 'User deleted successfully.'];
        }else{
            return ['Message' => 'You are not authorized.'];
        }
    }

  
    public function store(Request $request)
    {
        if (Gate::allows('isAdmin'))
        {
            $this->validate($request, [
                'name'      => 'required|string|max:191',
                'email'     => 'required|string|email|max:191|unique:users',
                'password'  => 'required|string|min:6',
                'type'      => 'required|string',
            ]);
    
            return User::firstorCreate([
                'name' => title_case($request->name),
                'email' => $request->email,
                'bio' => $request->bio,
                'type' => $request->type,
                'password' => Hash::make($request->password),
                'photo' => $request->photo,
                ]);
        } else{
            return ['Message' => 'You are not authorized.'];
        }
    }


    public function show($id)
    {
        //
    }

    public function profile()
    {
        return auth('api')->user();
    }


    public function updateProfile(Request $request)
    {
        $user = auth('api')->user();

        $this->validate($request, [
            'name'      => 'required|string|max:191',
            'email'     => 'required|string|email|max:191|unique:users,email,'.$user->id,
            'password'  => 'sometimes|string|min:6',
        ]);

        $presentprofilePhoto = $user->photo;

        if ($request->photo != $presentprofilePhoto) {
            $name = time().'.' . explode('/', explode(':', substr(
                $request->photo,
                0,
            strpos($request->photo, ';')
            ))[1])[1];

            Image::make($request->photo)->save(public_path('img/profile/').$name);
            $request->merge(['photo'     => $name, ]);

            $profilePhoto = public_path('img/profile/').$presentprofilePhoto;

            if (file_exists($profilePhoto)) {
                @unlink($profilePhoto);
            }
        }

        if (filled($request->password)) {
            $password = Hash::make($request->password);
            $request->merge(['password'  => $password]);
        }

        $user->update($request->all());

        return ['message' => 'Profile updated successfully.'];
    }


    public function update(Request $request, $id)
    {
        if (Gate::allows('isAdmin'))
        {
            $user = User::findOrfail($id);

            $this->validate($request, [
                'name'      => 'required|string|max:191',
                'email'     => 'required|string|email|max:191|unique:users,email,'.$user->id,
                'password'  => 'sometimes|min:6',
            ]);

            if (filled($request->password)) {
                $password = Hash::make($request->password);
                $request->merge(['password'  => $password]);
            }
            
            $user->update($request->all());

            return ['message' => 'User updated successfully.'];
        }else{
            return ['Message' => 'You are not authorized.'];
        }     
    }

    public function findBox()
    {
        if (Gate::allows('isAdmin'))
        {
            if (filled($search = \Request::get('q')))
            {
                $users = User::withTrashed()->where(
                    function ($query) use ($search)
                    {
                        $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('email', 'LIKE', "%$search%")
                                ->orWhere('type', 'LIKE', "%$search%");
                    })->paginate(10);
                
                return $users;
            } else {
                return ['Message' => 'Search failed due to empty query.'];
            }
        } else {
            return ['Message' => 'You are not authorized.'];
        }
    }

    
    public function destroy($id)
    {
        if (Gate::allows('isAdmin'))
        {
            $user = User::findOrfail($id);
            $user->delete();
            return ['message' => 'User deleted successfully.'];
        }else{
            return ['Message' => 'You are not authorized.'];
        }
    }
}
