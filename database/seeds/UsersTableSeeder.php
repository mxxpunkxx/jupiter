<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('users')->insert([
            
        //     'name' => str_random(10),
        //     'email' => str_random(10).'@faker.com',
        //     'password' => bcrypt('secret'),
        //     'type' => 'user',
        //     'bio' => str_random(30),
        //     'photo' => 'profile.png',

        //     ]);

        factory(App\User::class, 100)->create();
    }
}
